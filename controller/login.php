<?php
session_start();
require_once "../class/Usuario.php";

$login = $_POST["login"];

if($login) {

  $user = new Usuario;
  $user->setNome($login);
  $page = 'dashboard.php';

} else {

  $_SESSION["falha"] = 'Usuário incorreto';
  $page = 'index.php';

}

header("location: ../$page");
die();
