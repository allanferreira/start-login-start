<?php
session_start();
require_once "class/Usuario.php";

$user = new Usuario;
if($user->estaLogado()){
  header("location: dashboard.php");
  die();
}

require "inc/header.php";
?>

<form action="controller/login.php" method="post">
  <input name="login" type="text" placeholder="Login">
  <input type="submit" value="Entrar">
</form>

<?php require "inc/footer.php";
