<?php
session_start();
class Usuario {
  public function setNome($nome){
    $_SESSION['login'] = $nome;
  }

  public function getNome(){
    return $_SESSION['login'];
  }

  public function estaLogado(){
    return isset($_SESSION['login']);
  }

  public function desloga(){
    session_destroy();
  }
}
