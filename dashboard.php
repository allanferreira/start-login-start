<?php
session_start();
require_once "class/Usuario.php";

$user = new Usuario;
if(!$user->estaLogado()){
  header("location: index.php");
  die();
}

require "inc/header.php";

echo "<strong>{$user->getNome()}</strong> está logado";

require "inc/footer.php";
