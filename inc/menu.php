<?php
session_start();
require_once "class/Usuario.php";
$user = new Usuario;
if($user->estaLogado()){
?>
  <div>
    <a href="dashboard.php">Dashboard</a>
    <a href="controller/logout.php">Logout</a>
  </div>
<?php } else {?>
  <div>
    <a href="index.php">Home</a>
  </div>
<?php }
